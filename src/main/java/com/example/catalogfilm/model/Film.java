package com.example.catalogfilm.model;

import com.example.catalogfilm.constants.GenreEnum;
import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;


@Data
@Entity
public class Film {

    @Id
    @UuidGenerator
    private UUID uuid;

    private String title;
    private GenreEnum genre;
    private Integer rating;
}
