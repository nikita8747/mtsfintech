package com.example.catalogfilm.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import org.hibernate.annotations.UuidGenerator;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Entity
public class Director {

    @Id
    @UuidGenerator
    private UUID uuid;

    private String name;
    private Integer age;
    private String country;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Film> filmList;
}
